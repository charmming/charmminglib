#!/usr/bin/env python3

from setuptools import setup

setup(
    name='charmminglib',
    version='1.0.0',
    description='CHARMMing shared utils library',
    author='Joshua Smock',
    url='gitlab.com/charmming/charmminglib',
    packages=[
      'charmminglib'
    ],
    package_dir={'charmminglib': 'src/charmminglib'},
    install_requires=[
      'termcolor == 1.1.0',
      'cryptography >= 2.1.4',
      'msgpack-python >= 0.4.8'
    ],
    license='Public Domain',
    classifiers=[
      'Development Status :: 2 - Pre-Alpha',
      'Environment :: Console',
      'Intended Audience :: Science/Research',
      'License :: Public Domain',
      'Operating System :: POSIX :: Linux',
      'Programming Language :: Python'
    ]
  )
