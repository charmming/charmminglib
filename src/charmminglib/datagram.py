import msgpack

def pack(raw):
  return msgpack.packb(raw, use_bin_type=True)

def unpack(raw):
  return msgpack.unpackb(raw, encoding='utf-8')
