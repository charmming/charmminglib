from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

from typing import Optional

import os
import binascii

def encrypt(key: bytes, message: bytes) -> Optional[str]:
  padder = padding.PKCS7(128).padder()
  padded_secret_key = padder.update(key) + padder.finalize()

  padder = padding.PKCS7(128).padder()
  padded_session_id = padder.update(message) + padder.finalize()

  iv = os.urandom(16)

  cipher = Cipher(algorithms.AES(padded_secret_key), modes.CBC(iv), backend=default_backend())
  encryptor = cipher.encryptor()
  digest = encryptor.update(padded_session_id) + encryptor.finalize()

  iv_str = binascii.hexlify(iv).decode('utf-8')
  digest_str = binascii.hexlify(digest).decode('utf-8')

  return "{}{}".format(iv_str, digest_str)

def decrypt(key: bytes, encrypted_message: str) -> Optional[bytes]:
  padder = padding.PKCS7(128).padder()
  padded_secret_key = padder.update(key) + padder.finalize()

  iv = binascii.unhexlify(encrypted_message[:32])
  digest = binascii.unhexlify(encrypted_message[32:])

  cipher = Cipher(algorithms.AES(padded_secret_key), modes.CBC(iv), backend=default_backend())
  decryptor = cipher.decryptor()
  padded_session_id = decryptor.update(digest) + decryptor.finalize()

  unpadder = padding.PKCS7(128).unpadder()

  result = unpadder.update(padded_session_id) + unpadder.finalize()

  return result
